define([], function() {

  function Evented() {
    if (!this._e_eventListeners) {
      this._e_eventListeners = null;
    }
  }

  Evented.prototype = {
    emit: function(eventName, event) {

      var i, l, listeners;
      if (this._e_eventListeners) {//notify listeners registered with 'on()'
        listeners = this._e_eventListeners[eventName];
        if (listeners) {
          for (i = 0, l = listeners.length; i < l; i++) {
            try {
              listeners[i](event);
            } catch (e) {
              console.error(e);
            }
          }
        }
      }

      var onHandler = this['on' + eventName];//notify listener at this.oneventName (if any)
      if (typeof onHandler === 'function') {
        onHandler.call(this, event);
      }
    },
    on: function(eventName, callback) {

      if (!this._e_eventListeners) {
        this._e_eventListeners = {};
      }

      var listeners = this._e_eventListeners[eventName];
      if (!listeners) {
        listeners = [];
        this._e_eventListeners[eventName] = listeners;
      }
      listeners.push(callback);
      return {
        remove: function() {
          var index = listeners.indexOf(callback);
          if (index > -1) {
            listeners.splice(index, 1);
          }
        }
      };
    }
  };

  Evented.addEvented = function(ob) {
    for (var i in Evented.prototype) {
      ob[i] = Evented.prototype[i];
    }
    Evented.call(ob);
    return ob;
  };


  return Evented;

});